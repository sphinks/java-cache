package com.not4j.datasource;

/**
 * @Author: ivan
 * Date: 17.04.15
 * Time: 1:38
 */
public interface DataSource<K,V> {

    public V get(K key);
}
