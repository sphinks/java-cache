package com.not4j.datasource.impl;

import com.not4j.datasource.DataSource;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: ivan
 * Date: 17.04.15
 * Time: 1:39
 */
public class SimpleDataSourceImpl implements DataSource {


    public static final Map<String, Integer> topGoogleWords;

    static{
        topGoogleWords = new HashMap<String, Integer>();
        topGoogleWords.put("Robin Williams", 100);
        topGoogleWords.put("World Cup", 87);
        topGoogleWords.put("Ebola", 68);
        topGoogleWords.put("Malaysia Airlines", 64);
        topGoogleWords.put("ALS Ice Bucket Challenge", 57);
        topGoogleWords.put("Flappy Bird", 48);
        topGoogleWords.put("Conchita Wurst", 29);
        topGoogleWords.put("ISIS", 22);
        topGoogleWords.put("Frozen", 18);
        topGoogleWords.put("Sochi Olympics", 10);
    }

    @Override
    public Object get(Object key) {
        return topGoogleWords.get(key);
    }
}
