package com.not4j.cachelayer.impl;

import com.not4j.evict.EvictionPolicy;

import java.io.*;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: ivan
 * Date: 12.04.15
 * Time: 23:30
 */
public class SecondLvlCacheLayer<K,V> extends AbstractCacheLayer<K,V> {

    public static final String CACHE_FOLDER = "cache";
    private final int boundSize;
    private int cacheSize;
    private ConcurrentHashMap<K, UUID> fileNames;

    public SecondLvlCacheLayer(EvictionPolicy<K,V> evictionPolicy, int boundSize) {

        super(evictionPolicy);
        if (boundSize <= 0) {
            throw new IllegalArgumentException("Cache size should be greater than 0");
        }
        this.boundSize = boundSize;
        this.cacheSize = 0;
        this.fileNames = new ConcurrentHashMap<K, UUID>();

        File folder = new File(CACHE_FOLDER);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        cleanCacheFolder(folder);
    }

    @Override
    protected V performGet(K key) {

        V value = null;
        FileInputStream fis = null;
        ObjectInputStream oin = null;

        UUID fileName = fileNames.get(key);

        if (fileName == null) {
            return value;
        }

        File cacheFile = new File(CACHE_FOLDER + File.separatorChar + fileName);
        if(!cacheFile.exists()) {
            return value;
        }
        try {
            fis = new FileInputStream(cacheFile);
            oin = new ObjectInputStream(fis);
            value = (V)oin.readObject();
        }catch (IOException ex){
            System.out.println("Problem with file reading value with key " + key + ':' + ex.getMessage());
        }catch (ClassNotFoundException ex){
            System.out.println("Class does not support serialization:" + ex.getMessage());
        }finally {
            try {
                if (oin != null) {
                    oin.close();
                }
            }catch(IOException ex){
                System.out.println("Problem while closing object input stream: " + ex.getMessage());
            }
        }
        return value;
    }

    @Override
    protected synchronized void performPut(K key, V value) {

        if (cacheSize >= boundSize) {
            K evictKey = evictionPolicy.evict();
            remove(evictKey);
        }

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        UUID fileName = UUID.randomUUID();

        try {
            File cacheFile = new File(CACHE_FOLDER + File.separatorChar + fileName.toString());
            if(!cacheFile.exists()) {
                cacheFile.createNewFile();
            }

            fos = new FileOutputStream(cacheFile);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(value);
            cacheSize++;
            fileNames.put(key, fileName);
        }catch (IOException ex){
            System.out.println("Problem with file writing: " + ex.getMessage());
        }finally {
            try {
                if (oos != null) {
                    oos.close();
                }
            }catch(IOException ex){
                System.out.println("Problem while closing object output stream: " + ex.getMessage());
            }
        }
    }

    @Override
    public int getSize() {
        return cacheSize;
    }

    @Override
    public synchronized void remove(K key) {

        UUID fileName = fileNames.get(key);

        if (fileName != null) {
            File file = new File(CACHE_FOLDER + File.separatorChar + fileName.toString());
            if (file.exists()) {
                file.delete();
            }
            cacheSize--;
        }
    }

    public String getFileNameForKey(K key) {
        UUID fileName = fileNames.get(key);
        return fileName != null ? fileName.toString() : null;
    }

    private void cleanCacheFolder(File folder) {

        File[] files = folder.listFiles();
        if (files != null) {
            for (File f: files) {
                if (!f.isDirectory()) {
                    f.delete();
                }
            }
        }
    }
}
