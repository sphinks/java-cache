package com.not4j.cachelayer.impl;

import com.not4j.cachelayer.CacheLayer;
import com.not4j.evict.EvictionPolicy;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: ivan
 * Date: 11.04.15
 * Time: 15:15
 */
public class FirstLvlCacheLayer<K,V> extends AbstractCacheLayer<K,V> {

    private Map<K,V> memCache;
    private final int boundSize;
    private int cacheSize;
    private CacheLayer<K,V> nextLayer;

    public FirstLvlCacheLayer(EvictionPolicy evictionPolicy, int boundSize, CacheLayer<K,V> nextLayer) {

        super(evictionPolicy);
        this.memCache = new ConcurrentHashMap<K,V>();
        if (boundSize <= 0) {
            throw new IllegalArgumentException("Cache size should be greater than 0");
        }
        this.boundSize = boundSize;
        this.cacheSize = 0;
        this.nextLayer = nextLayer;
    }

    @Override
    protected V performGet(K key) {
        return memCache.get(key);
    }

    @Override
    protected synchronized void performPut(K key, V value) {

        if (cacheSize >= boundSize) {
            K evictKey = evictionPolicy.evict();
            if (nextLayer != null && memCache.containsKey(evictKey)) {
                nextLayer.put(evictKey, memCache.get(evictKey));
            }
            remove(evictKey);
        }
        memCache.put(key, value);
        cacheSize++;
    }

    @Override
    public int getSize() {
        return cacheSize;
    }

    @Override
    public synchronized void remove(K key) {
        if (memCache.containsKey(key)) {
            memCache.remove(key);
            cacheSize--;
        }
    }
}
