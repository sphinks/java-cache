package com.not4j.cachelayer.impl;

import com.not4j.cachelayer.CacheLayer;
import com.not4j.evict.EvictionPolicy;

/**
 * @Author: ivan
 * Date: 13.04.15
 * Time: 22:34
 */
public abstract class AbstractCacheLayer<K,V> implements CacheLayer<K,V> {

    protected EvictionPolicy<K,V> evictionPolicy;

    public AbstractCacheLayer(EvictionPolicy<K,V> evictionPolicy) {
        this.evictionPolicy = evictionPolicy;
    }

    @Override
    public V get(K key) {

        evictionPolicy.get(key);
        return performGet(key);
    }

    @Override
    public void put(K key, V value) {

        evictionPolicy.put(key,value);
        performPut(key,value);
    }

    protected abstract V performGet(K key);

    protected abstract void performPut(K key, V value);
}
