package com.not4j.cachelayer;

/**
 * @Author: ivan
 * Date: 12.04.15
 * Time: 23:31
 */
public interface CacheLayer<K,V> {

    public V get(K key);

    public void put(K key, V value);

    public int getSize();

    public void remove(K key);
}
