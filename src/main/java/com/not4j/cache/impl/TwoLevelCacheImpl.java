package com.not4j.cache.impl;

import com.not4j.cache.Cache;
import com.not4j.cachelayer.CacheLayer;
import com.not4j.cachelayer.impl.FirstLvlCacheLayer;
import com.not4j.cachelayer.impl.SecondLvlCacheLayer;
import com.not4j.datasource.DataSource;
import com.not4j.evict.impl.FifoEvictPolicy;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @Author: ivan
 * Date: 15.04.15
 * Time: 1:03
 */
public class TwoLevelCacheImpl<K,V> implements Cache<K,V> {

    private CacheLayer<K,V> firstLayer;
    private CacheLayer<K,V> secondLayer;
    private DataSource<K,V> dataSource;

    private AtomicLong firstLvlHits;
    private AtomicLong secondLvlHits;

    public TwoLevelCacheImpl(CacheLayer<K,V> firstLayer, CacheLayer<K,V> secondLayer, DataSource<K,V> dataSource) {
        this.firstLayer = firstLayer;
        this.secondLayer = secondLayer;
        this.dataSource = dataSource;
        firstLvlHits = new AtomicLong(0);
        secondLvlHits = new AtomicLong(0);
    }

    @Override
    public V get(K key) {
        V result = firstLayer.get(key);
        if (result == null) {
            result = secondLayer.get(key);
            if (result == null) {
                result = getKeyValueFromSource(key);
            }else{
                secondLvlHits.incrementAndGet();
            }
            if (result != null) {
                System.out.println(key);
                put(key, result);
            }
        }else{
            firstLvlHits.incrementAndGet();
        }
        return result;
    }

    @Override
    public void put(K key, V value) {
        firstLayer.put(key, value);
    }

    private V getKeyValueFromSource(K key) {
        try {
            Thread.sleep(2000);
        }catch(InterruptedException ex){
            System.out.println(ex.getMessage());
        }
        return dataSource.get(key);
    }

    public AtomicLong getFirstLvlHits() {
        return firstLvlHits;
    }

    public AtomicLong getSecondLvlHits() {
        return secondLvlHits;
    }
}
