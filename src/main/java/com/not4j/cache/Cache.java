package com.not4j.cache;

/**
 * @Author: ivan
 * Date: 15.04.15
 * Time: 0:09
 */
public interface Cache<K,V> {

    public V get(K key);

    public void put(K key, V value);
}
