package com.not4j.evict;

/**
 * @Author: ivan
 * Date: 11.04.15
 * Time: 15:55
 */
public interface EvictionPolicy<K,V> {

    public K evict();

    public void get(K key);

    public void put(K key, V value);
}
