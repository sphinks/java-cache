package com.not4j.evict.impl;

import com.not4j.evict.EvictionPolicy;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: ivan
 * Date: 17.04.15
 * Time: 0:43
 */
public class LRUEvictPolicy<K,V> implements EvictionPolicy<K,V> {

    private ConcurrentHashMap<K,Long> timeOfUse;

    public LRUEvictPolicy() {
        this.timeOfUse = new ConcurrentHashMap<K,Long>();
    }

    @Override
    public synchronized K evict() {

        K result = null;

        List<K> sortedKeys = new ArrayList<K>(timeOfUse.keySet());
        if (sortedKeys.size() > 0) {
            Collections.sort(sortedKeys, new Comparator<K>() {
                public int compare(K key1, K key2) {
                    return timeOfUse.get(key1).compareTo(timeOfUse.get(key2));
                }
            });


            result = sortedKeys.get(0);
            timeOfUse.remove(result);
        }
        return result;
    }

    @Override
    public void get(K key) {
        timeOfUse.replace(key, new Long(System.nanoTime()));
    }

    @Override
    public void put(K key, V value) {
        timeOfUse.put(key, new Long(System.nanoTime()));
    }
}
