package com.not4j.evict.impl;

import com.not4j.evict.EvictionPolicy;

import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @Author: ivan
 * Date: 11.04.15
 * Time: 15:57
 */
public class FifoEvictPolicy<K,V> implements EvictionPolicy<K,V> {

    private ConcurrentLinkedQueue<K> keys;

    public FifoEvictPolicy() {
        this.keys = new ConcurrentLinkedQueue<K>();
    }

    @Override
    public K evict() {
        return keys.poll();
    }

    @Override
    public void get(K key) {
    }

    @Override
    public void put(K key, V value) {
        keys.offer(key);
    }
}
