package com.not4j;

import com.not4j.cache.Cache;
import com.not4j.cache.impl.TwoLevelCacheImpl;
import com.not4j.cachelayer.CacheLayer;
import com.not4j.cachelayer.impl.FirstLvlCacheLayer;
import com.not4j.cachelayer.impl.SecondLvlCacheLayer;
import com.not4j.datasource.impl.SimpleDataSourceImpl;
import com.not4j.evict.EvictionPolicy;
import com.not4j.evict.impl.FifoEvictPolicy;
import com.not4j.evict.impl.LRUEvictPolicy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


public class App 
{
    public static void main( String[] args )
    {

        final int THREAD_NUMBER = 5;
        final int THREAD_CYCLES = 10;
        final CyclicBarrier startBarrier = new CyclicBarrier(THREAD_NUMBER);
        final CountDownLatch endLatch = new CountDownLatch(THREAD_NUMBER);
        final AtomicLong totalTime = new AtomicLong(0);
        final AtomicInteger notFoundErrors = new AtomicInteger(0);
        final Random randomGenerator = new Random();
        CacheLayer<String, Integer> secondLvl = new SecondLvlCacheLayer<String, Integer>(new FifoEvictPolicy<String, Integer>(), 3);
        CacheLayer<String, Integer> firstLvl = new FirstLvlCacheLayer<String, Integer>(new FifoEvictPolicy<String, Integer>(), 3, secondLvl);

        final Cache<String, Integer> cache = new TwoLevelCacheImpl<String, Integer>(firstLvl, secondLvl, new SimpleDataSourceImpl());



        for (int i = 0; i < THREAD_NUMBER; i++) {
            new Thread("New Thread #" + i) {
                public void run(){
                    try{
                        System.out.println("Thread " + Thread.currentThread().getName() + " awaits");
                        startBarrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Thread " + Thread.currentThread().getName() + " started");
                    for (int i = 0; i < THREAD_CYCLES; i++) {

                        //find what to ask from cache
                        List<String> keySet = new ArrayList<String>(SimpleDataSourceImpl.topGoogleWords.keySet());
                        String keyToRetrive = keySet.get(randomGenerator.nextInt(keySet.size()));

                        //cache work
                        long start = System.nanoTime();
                        Integer result = cache.get(keyToRetrive);
                        long end = System.nanoTime();

                        //valide cache result
                        if (!SimpleDataSourceImpl.topGoogleWords.get(keyToRetrive).equals(result)) {
                            notFoundErrors.incrementAndGet();
                        }

                        totalTime.addAndGet(end - start);
                    }
                    endLatch.countDown();
                }
            }.start();
        }
        try{
            endLatch.await();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Total time: " + totalTime.longValue() + " for " + THREAD_CYCLES * THREAD_NUMBER + " data requests");
        System.out.println("Time per request: " + ((double)totalTime.longValue()/(double)(THREAD_CYCLES * THREAD_NUMBER)) + " ns");
        System.out.println("First lvl hits: " + ((TwoLevelCacheImpl)cache).getFirstLvlHits() + ". Second lv hits: " + ((TwoLevelCacheImpl)cache).getSecondLvlHits());
        System.out.println("Errors of not equal result from cache: " + notFoundErrors.intValue());

    }
}
