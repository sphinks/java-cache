package com.not4j.cache.impl;

import com.not4j.cache.Cache;
import com.not4j.cachelayer.CacheLayer;
import com.not4j.cachelayer.impl.FirstLvlCacheLayer;
import com.not4j.cachelayer.impl.SecondLvlCacheLayer;
import com.not4j.datasource.DataSource;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * @Author: ivan
 * Date: 17.04.15
 * Time: 1:28
 */
public class TwoLevelCacheImplTest {

    private CacheLayer<String,String> firstLayer;
    private CacheLayer<String,String> secondLayer;
    private DataSource<String,String> dataSource;

    private final static String TEST_KEY1 = "Key1!";
    private final static String TEST_OBJECT1 = "Test object1!";

    @Before
    public void initTest() {
        firstLayer = mock(CacheLayer.class);
        secondLayer = mock(CacheLayer.class);
        dataSource = mock(DataSource.class);
    }

    @Test
    public void testGetWithDataSource() {

        Cache<String,String> cache = new TwoLevelCacheImpl<String,String>(firstLayer,secondLayer,dataSource);

        when(firstLayer.get(TEST_KEY1)).thenReturn(null);
        when(secondLayer.get(TEST_KEY1)).thenReturn(null);
        when(dataSource.get(TEST_KEY1)).thenReturn(TEST_OBJECT1);

        String result = cache.get(TEST_KEY1);

        assertEquals(TEST_OBJECT1, result);

        verify(firstLayer).get(TEST_KEY1);
        verify(secondLayer).get(TEST_KEY1);
        verify(dataSource).get(TEST_KEY1);

        verify(firstLayer).put(TEST_KEY1, TEST_OBJECT1);
    }

    @Test
    public void testGetWithSecondLayer() {

        Cache<String,String> cache = new TwoLevelCacheImpl<String,String>(firstLayer,secondLayer,dataSource);

        when(firstLayer.get(TEST_KEY1)).thenReturn(null);
        when(secondLayer.get(TEST_KEY1)).thenReturn(TEST_OBJECT1);

        String result = cache.get(TEST_KEY1);

        assertEquals(TEST_OBJECT1, result);

        verify(firstLayer).get(TEST_KEY1);
        verify(secondLayer).get(TEST_KEY1);
        verify(dataSource, never()).get(TEST_KEY1);

        verify(firstLayer).put(TEST_KEY1, TEST_OBJECT1);
    }

    @Test
    public void testGetWithFirstLayer() {

        Cache<String,String> cache = new TwoLevelCacheImpl<String,String>(firstLayer,secondLayer,dataSource);

        when(firstLayer.get(TEST_KEY1)).thenReturn(TEST_OBJECT1);

        String result = cache.get(TEST_KEY1);

        assertEquals(TEST_OBJECT1, result);

        verify(firstLayer).get(TEST_KEY1);
        verify(secondLayer, never()).get(TEST_KEY1);
        verify(dataSource, never()).get(TEST_KEY1);

        verify(firstLayer, never()).put(TEST_KEY1, TEST_OBJECT1);
    }

    @Test
    public void testGetWithoutData() {

        Cache<String,String> cache = new TwoLevelCacheImpl<String,String>(firstLayer,secondLayer,dataSource);

        when(firstLayer.get(TEST_KEY1)).thenReturn(null);
        when(secondLayer.get(TEST_KEY1)).thenReturn(null);
        when(dataSource.get(TEST_KEY1)).thenReturn(null);

        String result = cache.get(TEST_KEY1);

        assertNull(result);

        verify(firstLayer).get(TEST_KEY1);
        verify(secondLayer).get(TEST_KEY1);
        verify(dataSource).get(TEST_KEY1);

        verify(firstLayer, never()).put((String)anyVararg(), (String)anyVararg());
    }
}
