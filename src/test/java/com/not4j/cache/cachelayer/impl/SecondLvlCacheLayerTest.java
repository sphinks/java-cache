package com.not4j.cache.cachelayer.impl;

import com.not4j.cachelayer.impl.FirstLvlCacheLayer;
import com.not4j.cachelayer.impl.SecondLvlCacheLayer;
import com.not4j.evict.EvictionPolicy;
import com.not4j.evict.impl.FifoEvictPolicy;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @Author: ivan
 * Date: 13.04.15
 * Time: 0:10
 */
public class SecondLvlCacheLayerTest {

    private final static String TEST_KEY1 = "Key1!";
    private final static String TEST_OBJECT1 = "Test object1!";
    private final static String TEST_KEY2 = "Key2!";
    private final static String TEST_OBJECT2 = "Test object2!";
    private final static String TEST_KEY3 = "Key3!";
    private final static String TEST_OBJECT3 = "Test object3!";

    private EvictionPolicy<String,String> evictionPolicy = mock(EvictionPolicy.class);

    @Test
    public void testSimplePutAndGet() {

        SecondLvlCacheLayer<String,String> cache = new SecondLvlCacheLayer<String,String>(evictionPolicy, 2);
        assertEquals(0, cache.getSize());

        cache.put(TEST_KEY1, TEST_OBJECT1);
        assertEquals(1, cache.getSize());

        assertEquals(TEST_OBJECT1, cache.get(TEST_KEY1));
    }

    @Test
    public void testRemove() {

        SecondLvlCacheLayer<String,String> cache = new SecondLvlCacheLayer<String,String>(evictionPolicy, 2);
        File cacheFolder = new File(SecondLvlCacheLayer.CACHE_FOLDER);

        File[] fileList = cacheFolder.listFiles();
        assertNotNull(fileList);
        assertEquals(0, cacheFolder.listFiles().length);
        assertEquals(0, cache.getSize());

        cache.put(TEST_KEY1, TEST_OBJECT1);
        assertEquals(1, cache.getSize());
        File testFile = new File(SecondLvlCacheLayer.CACHE_FOLDER + "/" + cache.getFileNameForKey(TEST_KEY1));
        assertNotNull(cache.getFileNameForKey(TEST_KEY1));
        assertTrue(testFile.exists());

        cache.remove(TEST_KEY1);
        assertEquals(0, cache.getSize());
        assertNull(cache.get(TEST_KEY1));
        assertFalse(testFile.exists());
    }

    @Test
    public void testGetNameForKey() {
        SecondLvlCacheLayer<String,String> cache = new SecondLvlCacheLayer<String,String>(evictionPolicy, 2);
        cache.put(TEST_KEY1, TEST_OBJECT1);

        assertNotNull(cache.getFileNameForKey(TEST_KEY1));
        assertNull(cache.getFileNameForKey(TEST_KEY2));
    }

    @Test
    public void testBoundLimitWithEvictPolicy() throws InterruptedException {

        SecondLvlCacheLayer<String,String> cache = new SecondLvlCacheLayer<String,String>(evictionPolicy, 2);

        cache.put(TEST_KEY1, TEST_OBJECT1);
        cache.put(TEST_KEY3, TEST_OBJECT3);
        assertEquals(2, cache.getSize());

        when(evictionPolicy.evict()).thenReturn(TEST_KEY1);

        cache.put(TEST_KEY2, TEST_OBJECT2);
        assertEquals(2, cache.getSize());
        assertNull(cache.get(TEST_KEY1));

        when(evictionPolicy.evict()).thenReturn(TEST_KEY3);

        cache.put(TEST_KEY1, TEST_OBJECT1);
        assertEquals(2, cache.getSize());
        assertNull(cache.get(TEST_KEY3));

        verify(evictionPolicy, times(2)).evict();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBoundLimitEqualsZero() throws InterruptedException {

        SecondLvlCacheLayer<String,String> cache = new SecondLvlCacheLayer<String,String>(evictionPolicy, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBoundLimitLessZero() throws InterruptedException {

        SecondLvlCacheLayer<String,String> cache = new SecondLvlCacheLayer<String,String>(evictionPolicy, -5);
    }
}
