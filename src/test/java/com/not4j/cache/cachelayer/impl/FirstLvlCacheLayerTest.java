package com.not4j.cache.cachelayer.impl;

import com.not4j.cachelayer.impl.FirstLvlCacheLayer;
import com.not4j.cachelayer.impl.SecondLvlCacheLayer;
import com.not4j.evict.EvictionPolicy;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @Author: ivan
 * Date: 11.04.15
 * Time: 15:18
 */
public class FirstLvlCacheLayerTest {


    private final static String TEST_KEY1 = "Key1!";
    private final static String TEST_OBJECT1 = "Test object1!";
    private final static String TEST_KEY2 = "Key2!";
    private final static String TEST_OBJECT2 = "Test object2!";
    private final static String TEST_KEY3 = "Key3!";
    private final static String TEST_OBJECT3 = "Test object3!";

    private EvictionPolicy<String,String> evictionPolicy = mock(EvictionPolicy.class);

    @Test
    public void testSimplePutAndGet() {

        FirstLvlCacheLayer<String,String> cache = new FirstLvlCacheLayer<String,String>(evictionPolicy, 2, null);
        assertEquals(0, cache.getSize());

        cache.put(TEST_KEY1, TEST_OBJECT1);
        assertEquals(1, cache.getSize());

        assertEquals(TEST_OBJECT1, cache.get(TEST_KEY1));
    }

    @Test
    public void testRemove() {

        FirstLvlCacheLayer<String,String> cache = new FirstLvlCacheLayer<String,String>(evictionPolicy, 2, null);
        assertEquals(0, cache.getSize());

        cache.put(TEST_KEY1, TEST_OBJECT1);
        assertEquals(1, cache.getSize());
        cache.remove(TEST_KEY1);
        assertEquals(0, cache.getSize());
        assertNull(cache.get(TEST_KEY1));
    }

    @Test
    public void testRemoveWithWrongKey() {

        FirstLvlCacheLayer<String,String> cache = new FirstLvlCacheLayer<String,String>(evictionPolicy, 2, null);
        assertEquals(0, cache.getSize());

        cache.put(TEST_KEY1, TEST_OBJECT1);
        assertEquals(1, cache.getSize());
        cache.remove("NO_SUCH_KEY");
        assertEquals(1, cache.getSize());
        assertEquals(TEST_OBJECT1, cache.get(TEST_KEY1));
    }

    @Test
    public void testBoundSizeLimitAndNextLayer() {

        SecondLvlCacheLayer<String,String> cacheLayerMock = mock(SecondLvlCacheLayer.class);
        FirstLvlCacheLayer<String,String> cache = new FirstLvlCacheLayer<String,String>(evictionPolicy, 2, cacheLayerMock);

        assertEquals(0, cache.getSize());

        cache.put(TEST_KEY1, TEST_OBJECT1);
        cache.put(TEST_KEY2, TEST_OBJECT2);
        assertEquals(2, cache.getSize());

        when(evictionPolicy.evict()).thenReturn(TEST_KEY1);

        cache.put(TEST_KEY3, TEST_OBJECT3);
        assertEquals(2, cache.getSize());
        assertNull(cache.get(TEST_KEY1));

        verify(evictionPolicy).evict();
        verify(cacheLayerMock).put(TEST_KEY1,TEST_OBJECT1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBoundLimitEqualsZero() throws InterruptedException {

        FirstLvlCacheLayer<String,String> cache = new FirstLvlCacheLayer<String,String>(evictionPolicy, 0, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBoundLimitLessZero() throws InterruptedException {

        FirstLvlCacheLayer<String,String> cache = new FirstLvlCacheLayer<String,String>(evictionPolicy, -5, null);
    }
}
