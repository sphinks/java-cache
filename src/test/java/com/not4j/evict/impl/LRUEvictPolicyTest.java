package com.not4j.evict.impl;

import com.not4j.evict.EvictionPolicy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @Author: ivan
 * Date: 17.04.15
 * Time: 1:12
 */
public class LRUEvictPolicyTest {

    private final static String TEST_KEY1 = "Key1!";
    private final static String TEST_OBJECT1 = "Test object1!";
    private final static String TEST_KEY2 = "Key2!";
    private final static String TEST_OBJECT2 = "Test object2!";
    private final static String TEST_KEY3 = "Key3!";
    private final static String TEST_OBJECT3 = "Test object3!";

    EvictionPolicy<String,String> evictPolicy;

    @Test
    public void testSimpleEvict() {

        evictPolicy = new LRUEvictPolicy<String,String>();
        evictPolicy.put(TEST_KEY1, TEST_OBJECT1);
        evictPolicy.put(TEST_KEY2, TEST_OBJECT2);
        evictPolicy.put(TEST_KEY3, TEST_OBJECT3);

        String evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY1, evictKey);

        evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY2, evictKey);

        evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY3, evictKey);

    }

    @Test
    public void testEvictWithGetOnNotExistKey() {

        evictPolicy = new LRUEvictPolicy<String,String>();
        evictPolicy.put(TEST_KEY1, TEST_OBJECT1);
        evictPolicy.put(TEST_KEY2, TEST_OBJECT2);
        evictPolicy.put(TEST_KEY3, TEST_OBJECT3);

        String evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY1, evictKey);

        evictPolicy.get(TEST_KEY1);

        evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY2, evictKey);

        evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY3, evictKey);
    }

    @Test
    public void testEvictWithGets() {

        evictPolicy = new LRUEvictPolicy<String,String>();
        evictPolicy.put(TEST_KEY1, TEST_OBJECT1);
        evictPolicy.put(TEST_KEY2, TEST_OBJECT2);
        evictPolicy.put(TEST_KEY3, TEST_OBJECT3);

        evictPolicy.get(TEST_KEY1);

        String evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY2, evictKey);

        evictPolicy.get(TEST_KEY3);

        evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY1, evictKey);

        evictKey = evictPolicy.evict();
        assertEquals(TEST_KEY3, evictKey);
    }
}
