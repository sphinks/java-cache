package com.not4j.evict.impl;

import com.not4j.cachelayer.CacheLayer;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @Author: ivan
 * Date: 16.04.15
 * Time: 1:06
 */
public class FifoEvictPolicyTest {

    private final static String TEST_KEY1 = "Key1!";
    private final static String TEST_OBJECT1 = "Test object1!";
    private final static String TEST_KEY2 = "Key2!";
    private final static String TEST_OBJECT2 = "Test object2!";

    @Test
    public void testSimpleEvict() {

        FifoEvictPolicy<String,String> evictPolicy = new FifoEvictPolicy<String,String>();
        evictPolicy.put(TEST_KEY1, TEST_OBJECT1);
        evictPolicy.put(TEST_KEY2, TEST_OBJECT2);

        String evictKey = evictPolicy.evict();

        assertEquals(TEST_KEY1, evictKey);

    }
}
